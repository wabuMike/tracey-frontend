# Develop
1. install [VirtualBox](https://www.virtualbox.org/)
1. install [Vagrant](https://www.vagrantup.com/intro/getting-started/install)
1. clone this repository 
1. change directory to this repository
1. run `vagrant up`
1. run `vagrant ssh` to connect to the virtual machine. You now have access to the following utilities:
    * the source code repository is shared with the virtual machine and is located in `/vagrant/`
    * `vue`-command ([@vue/cli](https://cli.vuejs.org/))

## Develop with VSCode Remote SSH
1. follow instructions of [Remote development over SSH](https://code.visualstudio.com/docs/remote/ssh-tutorial)
1. use the following ssh-command: `ssh -F ~/.ssh/config vagrant@127.0.0.1`
1. once connected, from the "Explorer" view, choose "Open Folder". Use path `/vagrant`.

## Running web services in the Vagrant Box
Remember to bind web services in the Vagrant Box, which you want to access from your host, to IP `0.0.0.0` instead of to `localhost`. This makes them accessible through NAT. Example:
```
vue ui -H 0.0.0.0
```
The following ports are forwarded from the Box to the host:
* 8080
* 8000

# Docker image
## Run
1. Pull the image: `docker pull registry.gitlab.com/tracey2/tracey-frontend`
1. Run the image: `docker run -d -p 8080:8080 registry.gitlab.com/tracey2/tracey-frontend`

## Additional information
nginx is started with user `tracey` and uid=2002. If you want to mount a custom nginx configuration file into 
the container, make sure the uid has read access.