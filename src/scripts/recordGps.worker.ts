const ctx: Worker = self as any;

// Post data to parent thread
//ctx.postMessage({ foo: 'foo' });

// Respond to message from parent thread
//ctx.addEventListener('message', (event) => console.log(event));
ctx.onmessage = (event) => { 
  if(typeof event.data.start !== "undefined") {
    setInterval(() => ctx.postMessage({ time: Date.now() }));
  }
};