import { mount, Wrapper } from "@vue/test-utils";
import Vue from 'vue';
import RecordGps from "@/components/RecordGps.vue";
import Vuetify from 'vuetify'; 
import '@testing-library/jest-dom'

Vue.use(Vuetify);

describe("RecordGps.vue", () => {
  let vuetify: InstanceType<typeof Vuetify>;
  let wrapper: Wrapper<Vue>;

  beforeEach(() => {
    vuetify = new Vuetify();
    wrapper = mount(RecordGps, {vuetify});
  });
  it("start record button is available after loading", () => {
    // arrange
    const startRecording = wrapper.findComponent({ref: "startRecording"});

    // act
  
    // assert
    expect(startRecording.exists()).toBeTruthy();
    expect(startRecording.element).toBeVisible();
  });
  it("stop record button is not available after loading", () => {
    // arrange
    const stopRecording = wrapper.findComponent({ref: "stopRecording"});

    // act
  
    // assert
    expect(stopRecording.exists()).toBeFalsy();
  });
  it("click on start record button makes stop record button available", () => {
    // arrange
    //const startRecording = wrapper.findComponent({ref: "startRecording"});
    const startRecording = wrapper.findComponent({ref: "startRecording"});

    // act
    startRecording.trigger("click");
  
    // assert
    const recording = wrapper.findComponent({ref: "recording"});
    expect(recording.exists()).toBeTruthy();
    // const stopRecording = wrapper.find(".stopRecording");
    // expect(stopRecording.exists()).toBeTruthy();
    // expect(stopRecording.element).toBeVisible();
  });

  it("click on start record button makes start record button unavailable", () => {
    // arrange
    let startRecording: Wrapper<Vue>
    startRecording = wrapper.findComponent({ref: "startRecording"});

    // act
    startRecording.vm.$emit("click");
  
    // assert
    startRecording = wrapper.findComponent({ref: "startRecording"});
    expect(startRecording.exists()).toBeFalsy();
  });

  it("click on stop record button makes start record button available", () => {
    // arrange
    let startRecording: Wrapper<Vue>;
    startRecording = wrapper.findComponent({ref: "startRecording"});
    const stopRecording = wrapper.findComponent({ref: "stopRecording"});

    startRecording.vm.$emit("click");

    // act
    stopRecording.vm.$emit("click");
  
    // assert
    startRecording = wrapper.findComponent({ref: "startRecording"});
    expect(startRecording.exists()).toBeTruthy();
    expect(startRecording.element).toBeVisible();
  });

  it("click on stop record button makes stop record button unavailable", () => {
    // arrange
    let stopRecording: Wrapper<Vue>;
    const startRecording = wrapper.findComponent({ref: "startRecording"});
    stopRecording = wrapper.findComponent({ref: "stopRecording"});

    startRecording.vm.$emit("click");

    // act
    stopRecording.vm.$emit("click");
  
    // assert
    stopRecording = wrapper.findComponent({ref: "stopRecording"});
    expect(stopRecording.exists()).toBeFalsy();
  });
});
